from openjdk:8
RUN apt update && \
    apt install maven -y
RUN useradd jenkins
WORKDIR /root/.m2/
COPY settings.xml .

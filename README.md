# jenkins-shared-libraries



## Getting started

Very simple tests to check Jenkins shared library functions




## Test and Deploy

Use the built-in continuous integration in Jenkins or you can setup it in the local.


## Installation 
#####Tests runs on the docker containers, so the only requirement to hace a Docker installed on you machine


## Usage
*  clone the repository
* 	$ docker build -t *my-test-image-name* .
*  docker run -it -v `pwd`:/app/  *my-test-image-name* bash
*  cd /apps/ && mvn clean test

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.


## License
For open source projects, say how it is licensed.



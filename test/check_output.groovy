import com.homeaway.devtools.jenkins.testing.JenkinsPipelineSpecification

class CheckOutputSpec extends JenkinsPipelineSpecification {

    def checkoutput = null

    def setup() {
        checkoutput = loadPipelineScriptForTest("vars/check_output.groovy")
    }

    def "Test when deploy is true" () {
        when:
           checkoutput( true )
        then:
	       1 * getPipelineMock("sh")('echo deploy')
    }

    def "Test when deploy is false" () {
        when:
           checkoutput( false )
        then:
	       1 * getPipelineMock("sh")('echo nodeploy')
    }
}
